import com.example.tutorial.VehiclePositionProtos.VehiclePosition;
import java.io.*;
import java.util.*;

class AddVehiclePosition {
  // This function fills in a Person message based on user input.
  static VehiclePosition PromptForVehiclePosition(String id, float lat, float lon) throws IOException {
    VehiclePosition.Builder vehiclePosition = VehiclePosition.newBuilder();
    VehiclePosition.VehicleDescriptor.Builder vehicleDescriptor = VehiclePosition.VehicleDescriptor.newBuilder();
    vehicleDescriptor.setId(id);

    VehiclePosition.Position.Builder position = VehiclePosition.Position.newBuilder();
    position.setLatitude(lat);
    position.setLongtitude(lon);

    vehiclePosition.setTimestamp(0);
    vehiclePosition.setVehicle(vehicleDescriptor);
    vehiclePosition.setPosition(position);

    return vehiclePosition.build();
  }

  // Main function:  Reads the entire address book from a file,
  //   adds one person based on user input, then writes it back out to the same
  //   file.
  public static void main(String[] args) throws Exception {
    FileOutputStream output = new FileOutputStream(args[0]);

    try {

           Scanner s = new Scanner(new File("core.log"));
           ArrayList lines = new ArrayList();
           while (s.hasNextLine()) {
               String line = s.nextLine();
               if (line.contains("processing AvlReport") == true)
                   {
                     lines = new ArrayList();
                     do {
                     lines.add(line);
                     line = s.nextLine();
                   } while (!( line.equals("") ));
                   }

            //  System.out.println(lines);
              if ( lines.size() == 0 )
                continue;
              String message = "";
              Iterator it = lines.iterator();
              while ( it.hasNext() )
                {
                    message += (String)it.next();
                }
            //  System.out.println(message);
              int v1 = message.indexOf("vehicleId=");
              int v2 = message.indexOf(",");
              String id = message.substring(v1+10,v2);

              int l1 = message.indexOf("location=[");
              int l2 = message.indexOf("],");

              float lat = Float.parseFloat(message.substring(l1+10,l2).split(",")[0]);
              float lon = Float.parseFloat(message.substring(l1+10,l2).split(",")[1]);

              VehiclePosition vehiclePosition = AddVehiclePosition.PromptForVehiclePosition(id,lat,lon);
              vehiclePosition.writeTo(output);//build()
           }

           s.close();
       } catch (IOException ioex) {
           // handle exception...
       }
    output.close();
  }
}
